import unittest

class PyBaseTestingClass(unittest.TestCase):

	__monkeyPatches = []

	def setUp(self):
		super(PyBaseTestingClass, self).setUp()

	def tearDown(self):
		super(PyBaseTestingClass, self).tearDown()

		# remove monkey patchs
		for patch in self.__monkeyPatches:
			setattr(patch["orignalClass"], patch["oringalMethod"], patch["method"])
		self.__monkeyPatches = []

	def setTraceNose(self):
		from nose.tools import set_trace as nose_trace
		nose_trace()

	def setTracePyQt(self):
		from PyQt4.QtCore import pyqtRemoveInputHook
		from pdb import set_trace as pyqt_trace
		pyqtRemoveInputHook()
		pyqt_trace()

	def monkeyPatch(self, patchClass, patchMethod, replacementMethod):
		mpDict = {}
		mpDict["orignalClass"] = patchClass
		mpDict["oringalMethod"] = patchMethod
		mpDict["method"] = getattr(patchClass, patchMethod)	
		self.__monkeyPatches.append(mpDict)
		setattr(patchClass, patchMethod, replacementMethod)