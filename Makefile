# Makefile for PyMake, by Geoff Samuel

# Module Name		:	PyBase
# Description		:	Python tools that are used across modules as a central piliar
# Author			:	Geoff Samuel
# Support			:	Support@GeoffSamuel.com
# Wedsite			:	www.GeoffSamuel.com

version=1.1.1

target=PYTHON,OTHER

pythonSrc=python/
otherSrc=python/
otherTypes=xml,txt
convertPyQtUI=True
stringSubstute=0.1.0:$CALL(version)

centralDeploy=C:/pythonTools/deployed/$CALL(toolname)/versions/$CALL(versionToPy($CALL(version)))/

deployment=$CALL(fromEnv(PYMAKE_INSTALL))

dependencies=maya.cmds